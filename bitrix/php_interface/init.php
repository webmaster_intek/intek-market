<?php

use Bitrix\Main;
use Bitrix\Main\Mail\Event;

// Функция в подключении компонента bitrix:catalog
// function inmartResetIBlock(&$iblock_id, &$iblock_name)
// {
//     global $APPLICATION;
//     if (isset($_GET["SHOP_ID"]) && $_GET["SHOP_ID"] != $iblock_id) {
//         $shopArr = getMyInmartSuperConfigArray();
//         foreach ($shopArr as $shop) {
//             if ($_GET["SHOP_ID"] == $shop['iblock_id']) {
//                 $APPLICATION->set_cookie("SHOP_ID", $shop['iblock_id'], time()+60*60*24*30*12, "/");
//                 $APPLICATION->set_cookie("SHOP_NAME", $shop['title'], time()+60*60*24*30*12, "/");
//                 $iblock_id = $shop['iblock_id'];
//                 $iblock_name = $shop['title'];
//                 BXClearCache( true, "/");
//             }
//         }
//     }
// }
