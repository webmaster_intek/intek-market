<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;

Loader::includeModule("iblock");
Loader::includeModule("catalog");

global $DB;

$bs = new CIBlockSection;
$columns = "`pid`,`sort`,`name`,`region`,`city`,`street`,`house`,`descr`,`contact_person`,`tel`,`email`,`manager`,`foto`,`coords`,`randhash`,`activity`";
if ( ($handle_o = fopen($_SERVER["DOCUMENT_ROOT"]."/local/help/partners_20200331.csv", "r") ) !== FALSE ) {
    while ( ($data_o = fgetcsv($handle_o, 1000, ";")) !== FALSE) {
      $insertValues = array();
      array_shift($data_o);
      foreach( $data_o as $v ) {
         $insertValues[]="'".addslashes(trim($v))."'";
      }
      $values = implode(',',$insertValues);
      $values = iconv('CP1251', 'UTF-8', $values);

      $sql = "INSERT INTO `partners_adresses` ($columns) VALUES ($values)";
      $res = $DB->Query($sql, false, $err_mess.__LINE__);
    }
}
fclose($handle_o);

// if ( ($handle_o = fopen($_SERVER["DOCUMENT_ROOT"]."/local/help/partners_20200331.csv", "r") ) !== FALSE ) {
//     // читаем первую строку и разбираем названия полей
//     $columns_o = fgetcsv($handle_o, 1000, ";");
//     foreach( $columns_o as $v ) {
//        $insertColumns[]="'".addslashes(trim($v))."'";
//     }
//     $columns=implode(';',$insertColumns);
//
//
//     while ( ($data_o = fgetcsv($handle_o, 1000, ";")) !== FALSE) {
//       $insertValues = array();
//       foreach( $data_o as $v ) {
//          $insertValues[]="'".addslashes(trim($v))."'";
//       }
//       $values=implode(',',$insertValues);
//       $sql = "INSERT INTO `partners_adresses` ( $columns ) VALUES ( $values )";
//       $res = $DB->Query($sql, false, $err_mess.__LINE__);
//     }
//
// }
// fclose($handle_o);

//
// if (($handle = fopen($_SERVER["DOCUMENT_ROOT"]."/local/help/partners_20200331.csv", "r")) !== FALSE) {
//     while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
//         $num = count($data);
//         for ($c=0; $c < $num; $c++) {
//             echo "<p>".iconv('CP1251', 'UTF-8', $data[$c])."</p>";
//         }
//     }
//     fclose($handle);
// }
