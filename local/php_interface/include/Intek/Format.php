<?
namespace Intek;

class Format
{
    public static function Dates($dateString = "")
    {
        $date = strtotime($dateString);
        $months = array('', 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
        return date('d', $date) . '  ' . $months[(int) date('m', $date)];
    }
}
