<?
$arPerPages = [12, 24, 36, 48];

  if (isset($_REQUEST['orderBy'])) {
      if ($_REQUEST['orderBy'] == 'asc') {
          $orderBy = 'desc';
      } else {
          $orderBy = 'asc';
      }
  } else {
      $orderBy = 'asc';
  }

  if (isset($_REQUEST['sortBy'])) {
      $sortBy = $_REQUEST['sortBy'];
  } else {
      $sortBy = 'sort';
  }
  if ($sortBy == 'price') {
      $sortBy = 'CATALOG_PRICE_1';
  }
  if ($sortBy == 'date') {
      $sortBy = 'TIMESTAMP_X';
  }

?>

<div class="filters-sort">
    <div class="label">Сортировать:</div>
      <ul>
          <li <? if ($sortBy == 'CATALOG_PRICE_1') : ?> class="active" <? endif; ?>>
              <a rel="nofollow" href="<?= $APPLICATION->GetCurPageParam('sortBy=price&orderBy='.$orderBy, array('sortBy', 'orderBy')) ?>" <? if ($sortBy == 'price') : ?> class="active" <? endif; ?> data-order="<?php echo ($orderBy); ?>" >Цене</a>
          </li>

          <li <? if ($sortBy == 'name') : ?> class="active" <? endif; ?>>
              <a rel="nofollow" href="<?= $APPLICATION->GetCurPageParam('sortBy=name&orderBy='.$orderBy, array('sortBy', 'orderBy')) ?>" <? if ($sortBy == 'name') : ?> class="current-sort" <? endif; ?>data-order="<?php echo ($orderBy); ?>" >Наименованию</a>
          </li>

          <li <? if ($sortBy == 'TIMESTAMP_X') : ?> class="active" <? endif; ?>>
              <a rel="nofollow" href="<?= $APPLICATION->GetCurPageParam('sortBy=date&orderBy='.$orderBy, array('sortBy', 'orderBy')) ?>" <? if ($sortBy == 'date') : ?> class="current-sort" <? endif; ?> data-order="<?php echo ($orderBy); ?>" >Дате</a>
          </li>
      </ul>

    <div class="label hidden-md hidden-sm hidden-xs">Показывать по</div>
    <?
		$pageElementCount = $arParams["PAGE_ELEMENT_COUNT"];
		if (array_key_exists("showBy", $_REQUEST)) {
		    if ( intVal($_REQUEST["showBy"]) && in_array(intVal($_REQUEST["showBy"]), $arPerPages) ) {
		        $pageElementCount = intVal($_REQUEST["showBy"]);
		        $_SESSION["showBy"] = $pageElementCount;
		    } elseif ($_SESSION["showBy"]) {
		        $pageElementCount = intVal($_SESSION["showBy"]);
		    }
		}
		?>
      <ul class="hidden-md hidden-sm hidden-xs">
          <?php foreach ($arPerPages as $perPage) { ?>
              <li class="<?= $_REQUEST["showBy"] == $perPage ? 'active' : '' ?> --per-page">
                  <a href="<?= $APPLICATION->GetCurPageParam('showBy='.$perPage, array('showBy', 'mode')) ?>" data-action="set-user" data-code="PER_PAGE" data-value="<?= $perPage ?>" data-reload="yes">
                      <?= $perPage ?>
                  </a>
              </li>
          <?php } ?>
      </ul>
</div>
<style>
    .filters-section {
       position: relative;
       margin-bottom: 17px;
    }

    .filters-sort .label {
       display: inline-block;
       vertical-align: top;
       font-weight: 300;
       font-size: 12px;
       color: #979ea7;
    }

    .filters-sort>ul {
       display: inline-block;
       vertical-align: top;
    }

    .filters-sort li {
       display: inline-block;
       vertical-align: top;
       margin-right: 23px;
    }

    .filters-sort li a {
       position: relative;
       display: block;
       font-size: 12px;
       font-weight: 300;
       color: #222;
       padding-right: 11px;
       -webkit-transition: 0.15s;
       transition: 0.15s;
    }

    .filters-sort li a:hover,
    .filters-sort li.active a {
       color: #007bc6;
    }

    .filters-sort li a:hover:after {
       border-top: 5px solid #007bc6;
    }
    .filters-sort li.active a:after {
        border-top-color: #007bc6;
    }
    .filters-sort li.active a[data-order="asc"]:after,
    .filters-sort li.active a[data-order="ASC"]:after {
       -webkit-transform: scaleY(-1) translateY(3px);
       transform: scaleY(-1) translateY(3px);
    }


    .filters-sort li a:after {
       content: '';
       border: 3px solid transparent;
       border-top: 5px solid #979ea7;
       display: block;
       width: 0;
       height: 0;
       position: absolute;
       right: 0;
       top: 50%;
       margin-top: -2px;
       -webkit-transition: 0.15s;
       transition: 0.15s;
    }
    .filters-sort li.--per-page {
        margin-right: 5px;

    }

    .filters-sort li.--per-page a {
        padding-right: 0;
    }

    .filters-sort li.--per-page a:after {
        display: none;
    }
</style>
