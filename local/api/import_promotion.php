<?php
define("NO_KEEP_STATISTIC", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
Loader::includeModule("iblock");
Loader::includeModule("catalog");

global $USER, $DB;

while (ob_get_level()) {
	ob_end_flush();
}

if (!function_exists('getallheaders')) {
    function getallheaders()
    {
        $headers = array ();
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}
$headers = getallheaders();
if ($headers["Content-Type"] == "application/json") {
    $str_json = json_decode(file_get_contents('php://input'), true) ?: [];
	processPromotion($str_json);
}

function createPromotion($promotion_items=array(), $promotion_name='')
{
	$el = new CIBlockElement;
	$PROP = array();
	foreach ($promotion_items as $key => $promotion_item) {
		$product = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 3, 'XML_ID' => $promotion_item['item_xmlid'], 'ACTIVE' => 'Y'), false, array(), array('ID'))->Fetch();
		if ($product) {
			$arProperty[] = $product['ID'];
		}
	}
	$PROP[23] = $arProperty;
	$active = ($promotion_items[0]['date_end'] > date('d.m.Y H:i:s')) ? 'Y' : 'N';
	$arParams = array("replace_space"=>"-","replace_other"=>"-");
	$trans = Cutil::translit($promotion_name,"ru",$arParams);
	$arLoadProductArray = array(
		"MODIFIED_BY"    	=> 'PromotionLoader',
		"IBLOCK_ID"      	=> 5,
		"PROPERTY_VALUES"	=> $PROP,
		"NAME"           	=> trim($promotion_name),
		"ACTIVE"         	=> $active,
		"DATE_ACTIVE_FROM"  => $promotion_items[0]['date_begin'],
		"DATE_ACTIVE_TO"    => $promotion_items[0]['date_end'],
		"XML_ID"		 	=> $promotion_items[0]['onec_id'],
		"CODE"           	=> $trans,
	);
	if ($promotion_id = $el->Add($arLoadProductArray)) {
		Bitrix\Main\Diag\Debug::dumpToFile(date("Y.m.d H:i:s").' - success create - '.$promotion_id,"","/local/logs/import_promotion.txt");
	}

	return 0;
}

function updatePromotion($promotion_items=array(), $promotion_id=0)
{
	$el = new CIBlockElement;
	$arProperty = array();
	if ($promotion_items) {
		foreach ($promotion_items as $key => $promotion_item) {
			$product = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 3, 'XML_ID' => $promotion_item['item_xmlid'], 'ACTIVE' => 'Y'), false, array(), array('ID'))->Fetch();
			if ($product) {
				$arProperty[] = $product['ID'];
			}
		}
		if ($arProperty) {
			$el->Update($promotion_id, array(
				"ACTIVE" 			=> ($promotion_items[0]['date_end'] > date('d.m.Y H:i:s')) ? 'Y' : 'N',
				"DATE_ACTIVE_FROM" 	=> $promotion_items[0]['date_begin'],
				"DATE_ACTIVE_TO" 	=> $promotion_items[0]['date_end'],
				"PROPERTY_VALUES"	=> array(23 => $arProperty),
			));
			Bitrix\Main\Diag\Debug::dumpToFile(array(date("Y.m.d H:i:s"), $arProperty),"","/local/logs/import_promotion.txt");
		}
	}

	return 0;
}

function processPromotion($promotions)
{
	foreach ($promotions as $key => $promotion_items) {
		// if ($key == 'Сезон ДАЧЖ Дачная Жизнь') {
			$arSelect = array("ID");
			$arFilter = array("IBLOCK_ID"=> 5, "XML_ID" => $promotion_items[0]['onec_id']);
			$res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect)->Fetch();
			if ($res['ID']) {
				updatePromotion($promotion_items, $res['ID']);
			} else {
				createPromotion($promotion_items, $key);
			}
		// }
	}

	return 0;
}
