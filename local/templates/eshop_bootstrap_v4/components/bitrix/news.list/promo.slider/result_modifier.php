<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ($arResult['ITEMS'] as $key => $arItem) {
    if ($arItem['PROPERTIES']['PRODUCTS']['VALUE']) {
        foreach ($arItem['PROPERTIES']['PRODUCTS']['VALUE'] as $arProductID) {
            $product = CIBlockElement::GetByID($arProductID)->GetNext();
            $image = \CFile::ResizeImageGet($product['DETAIL_PICTURE'], array(), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, false);
            $product['IMAGE_SRC'] = $image['src'];

            $db_res = CPrice::GetList(array(), array("PRODUCT_ID" => $arProductID))->Fetch();
            $product['PRICE'] = CurrencyFormat($db_res["PRICE"], $db_res["CURRENCY"]);

            $arResult['ITEMS'][$key]['PRODUCTS'][] = $product;
        }
        shuffle($arResult['ITEMS'][$key]['PRODUCTS']);
    }
}
