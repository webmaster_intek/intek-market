<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="row owl-carousel owl-theme promo-item mt-5 mb-5">
    <? foreach ($arResult["ITEMS"] as $arItem) : ?>
        <?//Bitrix\Main\Diag\Debug::dumpToFile(array(date("Y.m.d H:i:s"), $arItem),"","/local/logs/test.txt");
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <div class="slider-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>"<? if ($arItem['PROPERTIES']['COLOR']['VALUE']):?> style="color:#<?=$arItem['PROPERTIES']['COLOR']['VALUE'];?>"<? endif;?>>
            <div class="col-12 col-md-8 promo-item-left">
                <p><img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" width="100%"></p>
                <h3 class="text-center"><?=$arItem['NAME']?></h3>
                <p class="text-center"><a href="<?=$arItem['DETAIL_PAGE_URL']?>">Подробности акции</a></p>
            </div>
            <div class="col-12 col-md-4 promo-item-right">
                <div class="row">
                    <? foreach ($arItem["PRODUCTS"] as $key => $arProduct) : ?>
                        <? if ($key < 4) : ?>
                            <div class="col-6 promo-item-right-product">
                                <a href="<?=$arProduct['DETAIL_PAGE_URL'];?>">
                                    <img class="mb-2" src="<?=$arProduct['IMAGE_SRC'];?>" alt="<?=$arProduct["NAME"]?>" width="100%">
                                    <p class="promo-item-right-product-title"><?=$arProduct['NAME']?></p>
                                    <p class="promo-item-right-product-price"><?=$arProduct["PRICE"]?></p>
                                </a>
                            </div>
                        <? endif; ?>
                    <? endforeach; ?>
                </div>
            </div>
        </div>
    <? endforeach; ?>
</div>

<script type="text/javascript">
  $('.owl-carousel').owlCarousel({
    items:1,
    // center: true,
    autoplayTimeout: 10000,
    // loop: true,
    autoplay: true,
    // nav: true,
    // navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>']
  });
</script>
