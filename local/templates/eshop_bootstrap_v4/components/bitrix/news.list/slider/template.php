<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="owl-carousel owl-theme mt-5">
<? foreach($arResult["ITEMS"] as $arItem){?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="slider-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>"<? if ($arItem['PROPERTIES']['COLOR']['VALUE']):?> style="color:#<?=$arItem['PROPERTIES']['COLOR']['VALUE'];?>"<? endif;?>>
        <?
        $pcBannerClass = "";
        $tableBannerClass = "";
        if ($arItem['PROPERTIES']['MOBILE_BANNER']['VALUE']):
            $pcBannerClass .= " d-xs-none";
            $tableBannerClass = " d-xs-none";
        ?>
        <div class="slider-item--banner d-xs-block d-none" style="background-image: url(<?=CFile::GetPath($arItem['PROPERTIES']['MOBILE_BANNER']['VALUE']);?>)"></div>
        <? endif;
        if ($arItem['PROPERTIES']['TABLET_BANNER']['VALUE']):
            $pcBannerClass = " d-sm-none";
        ?>
        <div class="slider-item--banner d-sm-block d-none<?=$tableBannerClass;?>" style="background-image: url(<?=CFile::GetPath($arItem['PROPERTIES']['TABLET_BANNER']['VALUE']);?>)"></div>
        <? endif;?>

        <a href="<?php if(is_array($arItem["DISPLAY_PROPERTIES"]['SLIDER_LINK'])) echo $arItem['DISPLAY_PROPERTIES']['SLIDER_LINK']['VALUE']; else echo '/'; ?>"><div class="slider-item--banner<?=$pcBannerClass;?>" style="background-image: url(<?=$arItem["PREVIEW_PICTURE"]["SRC"];?>)"></div></a>
    </div>
<?}?>
</div>

<script type="text/javascript">
  $('.owl-carousel').owlCarousel({
    items:1,
    // center: true,
    autoplayTimeout: 2700,
    loop: true,
    autoplay: true,
    nav: true,
    // navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>']
  });
</script>
