<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult))
	return;
?>

<?if (!empty($arResult)):?>
		<nav class="navbar mb-0">
			<ul class="nav list-inline d-none d-sm-flex">
<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
		continue;
?>
	<?if($arItem["SELECTED"]):?>
		<li class="mr-3 active"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
		<li class="mr-3 nav-item"><a class="text-secondary underline-link nav-link text-left pl-0 pr-0	" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>

<?endforeach?>
			</ul>
			<ul class="nav nav-mobile list-inline d-block d-sm-none">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="navbarDropdownMenuLink" href="#">
						Меню
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<?
						foreach($arResult as $arItem):
							if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1)
								continue;
						?>
						<?if($arItem["SELECTED"]):?>
						<a class="dropdown-item active" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
						<?else:?>
						<a class="dropdown-item text-secondary p-3 ml-3" href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a>
						<?endif?>
						<?endforeach?>
					</div>
				</li>
			</ul>
		</nav>
<?endif?>
