<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if ($arResult["DETAIL_PICTURE"])
    $arResult['IMAGE'] = $arResult["DETAIL_PICTURE"]['SRC'];

$arCurrent = $arParams['CURRENT'];
$arProductIds = [];
foreach ($arResult['PROPERTIES']['PRODUCTS']['VALUE'] as $product)
    $arProductIds[] = $product['PRODUCT'];
?>
<section class="category-title py-10">
    <div class="wrap">
        <h1 class="mb-0 pl-0"><?=$arResult['NAME'];?></h1>
    </div>
</section>

<? 
if ($arProductIds):
    $filterName = "arFavoriteFilter";
    global ${$filterName};
    ${$filterName}["=ID"] = $arProductIds;
    ?>
<div class="row promo-catalog-items">
    <div class="col-12 mt-10 text-center py-20">
        <div class="download-link">
            <a href="/upload/promo/<?=($arResult['CODE']);?>.xlsx" target="_blank" class="link"><span class="text">Скачать каталог</span> <span class="icon icon-excel"></span></a>
        </div>
    </div>
</div>
    <section class="catalog-section pt-0 mt-0">
        <div class="wrap">
            <div class="catalog_block">
                <div class="row">
                    <div class="col-3 col-md-12">
                        <div class="filters-aside d-md-none">
                            <div class="promo-item">
                                <? if ($arResult['PROPERTIES']['DISCOUNT']['VALUE']):?>
                                <div class="discount"><?=$arResult['PROPERTIES']['DISCOUNT']['VALUE'];?></div>
                                <? endif;?>
                                <div class="text"><?=$arResult['PREVIEW_TEXT'];?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-9 col-md-12">
                        <div class="catalog-content">
                            <div class="filters-section">                            
                                <? include($_SERVER["DOCUMENT_ROOT"] . INCLUDE_PATH . "/catalog.section/sort.php");?>                        
                                <? include($_SERVER["DOCUMENT_ROOT"] . INCLUDE_PATH . "/catalog.section/view.php");?>
                            </div>
                            <div class="catalog-items view-<?=$arCurrent['VIEW'];?>">
                                <?
                                $APPLICATION->IncludeComponent(
                                    "bitrix:catalog.section",
                                    "catalog",
                                    array(
                                        "IBLOCK_TYPE" => "1c_catalog",
                                        "IBLOCK_ID" => "1",
                                        "ELEMENT_SORT_FIELD" => $arCurrent["SORT_BY"],
                                        "ELEMENT_SORT_ORDER" => $arCurrent["SORT_ORDER"],
                                        "ELEMENT_SORT_FIELD2" => "id",
                                        "ELEMENT_SORT_ORDER2" => "desc",
                                        "PAGE_ELEMENT_COUNT" =>  count($arProductIds),
                                        "LINE_ELEMENT_COUNT" => 3,
                                        "PROPERTY_CODE" =>  array(
                                            0 => "INV_NUM",
                                            1 => "TM",
                                            2 => "COUNTRY",
                                            3 => "IN_PACK",
                                            4 => "CN_99990010038550208",
                                            5 => "CN_99990010038555596",
                                            6 => "CN_99990010038550168",
                                            7 => "CN_99990010038550167",
                                            8 => "CN_99990010038555600",
                                            9 => "CN_99990010038555607",
                                            10 => "CN_99990010038555615",
                                            11 => "CN_99990010038555616",
                                            12 => "CN_99990010038555631",
                                            13 => "BC",
                                            14 => "CML2_ARTICLE",
                                            15 => "BONUS",
                                            16 => "MATERIAL",
                                            17 => "SERIES",
                                            18 => "PACKAGE",
                                            19 => "",
                                        ),
                                        "PRODUCT_ID_VARIABLE" => "id",
                                        "SECTION_ID_VARIABLE" => "SECTION_ID",
                                        "CACHE_TYPE" => "A",
                                        "CACHE_TIME" => "36000000",
                                        "PRICE_CODE" => array(
                                            0 => "BASE",
                                            1 => "OPT",
                                        ),
                                        "USE_PRICE_COUNT" => "N",
                                        "SHOW_PRICE_COUNT" => "1",
                                        "PRICE_VAT_INCLUDE" => "Y",
                                        "USE_PRODUCT_QUANTITY" => "Y",
                                        "CONVERT_CURRENCY" => "Y",
                                        "CURRENCY_ID" => "RUB",
                                        "HIDE_NOT_AVAILABLE" => "Y",
                                        "DISPLAY_TOP_PAGER" => "N",
                                        "DISPLAY_BOTTOM_PAGER" => "N",
                                        "FILTER_NAME" => $filterName,
                                        "SECTION_CODE" => "",
                                        "SECTION_USER_FIELDS" => array(),
                                        "INCLUDE_SUBSECTIONS" => "Y",
                                        "SHOW_ALL_WO_SECTION" => "Y",
                                        "META_KEYWORDS" => "",
                                        "META_DESCRIPTION" => "",
                                        "BROWSER_TITLE" => "",
                                        "ADD_SECTIONS_CHAIN" => "N",
                                        "SET_TITLE" => "N",
                                        "SET_STATUS_404" => "N",
                                        "CACHE_FILTER" => "N",
                                        "CACHE_GROUPS" => "N",
                                    ),
                                    false
                                );?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? endif;?>