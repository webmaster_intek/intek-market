<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if ($arResult["DETAIL_PICTURE"])
    $arResult['IMAGE'] = $arResult["DETAIL_PICTURE"]['SRC'];
/*elseif ($arResult["PREVIEW_PICTURE"])
    $arResult['IMAGE'] = $arResult["PREVIEW_PICTURE"]['SRC'];*/
?>
<section class="news-page">
    <div class="wrap">
        <? if ($arResult['IMAGE']): ?>
        <h1 class="h2 mb-10"><?=$arResult['NAME'];?></h1>
        <div class="h5 mb-30"><?=strtolower($arResult["DISPLAY_ACTIVE_FROM"]);?></div>
        <? endif; ?>
        <div class="news-article">
            <div class="news-title<? if ($arResult['IMAGE']):?> has-bg" style="background-image: url(<?=$arResult['IMAGE'];?>)<? endif;?>">
                <? if ($arResult['PROPERTIES']['TYPE']['VALUE'] && $arResult['IMAGE']): ?>
                <div class="tag color-<?=$arResult['PROPERTIES']['TYPE']['VALUE_XML_ID'];?>"><?=$arResult['PROPERTIES']['TYPE']['VALUE'];?></div>
                <? endif; ?>
                <? if (!$arResult['IMAGE']): ?>
                <div class="inner">
                    <ul class="info">
                        <? if ($arResult['PROPERTIES']['TYPE']['VALUE']): ?>
                        <li><?=$arResult['PROPERTIES']['TYPE']['VALUE'];?></li>
                        <? endif; ?>
                        <li><?=strtolower($arResult["DISPLAY_ACTIVE_FROM"]);?></li>
                    </ul>
                    <h1><?=$arResult['NAME'];?></h1>
                </div>
                <? endif;?>
            </div>
            <article class="text_block">
            <? if (strlen($arResult["DETAIL_TEXT"]) > 0): ?>
                <?=$arResult["DETAIL_TEXT"];?>
            <? else: ?>
                <?=$arResult["PREVIEW_TEXT"];?>
            <? endif; ?>
            </article>

            <div class="news-bottom row align-items-center">
                <div class="col-6 col-sm-12">
                    <? if ($arResult['PROPERTIES']['PRODUCTS']['VALUE']): ?>
                    <div class="product-list">
                        <a href="<?=$arResult['DETAIL_PAGE_URL'];?>products/">Прайс лист</a>
                    </div>
                    <? endif;?>
                </div>
            </div>

        </div>
    </div>
</section>
