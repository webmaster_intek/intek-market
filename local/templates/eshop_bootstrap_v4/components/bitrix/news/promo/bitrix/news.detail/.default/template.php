<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if ($arResult["DETAIL_PICTURE"])
    $arResult['IMAGE'] = $arResult["DETAIL_PICTURE"]['SRC'];
/*elseif ($arResult["PREVIEW_PICTURE"])
    $arResult['IMAGE'] = $arResult["PREVIEW_PICTURE"]['SRC'];*/
?>
<section class="promo-page">
    <div class="wrap">
        <? if ($arResult['IMAGE']): ?>
        <h1 class="h2 mb-10"><?=$arResult['NAME'];?></h1>
        <div class="h5 mb-30">
            <svg class="icon-svg">
                <use xlink:href="/local/media/img/icons/icons.svg#calendar"></use>
            </svg>
            <span>Акция продлится <strong>с <?=$arResult["DATE_ACTIVE_FROM"]?> по <?=$arResult["DATE_ACTIVE_TO"]?></strong></span>
        </div>
        <? endif; ?>
        <div class="news-article">
            <div class="news-title<? if ($arResult['IMAGE']):?> has-bg" style="background-image: url(<?=$arResult['IMAGE'];?>)<? endif;?>">
                <? if ($arResult['PROPERTIES']['TYPE']['VALUE'] && $arResult['IMAGE']): ?>
                <div class="tag color-<?=$arResult['PROPERTIES']['TYPE']['VALUE_XML_ID'];?>"><?=$arResult['PROPERTIES']['TYPE']['VALUE'];?></div>
                <? endif; ?>
                <? if (!$arResult['IMAGE']): ?>
                <div class="inner">
                    <ul class="info">
                        <? if ($arResult['PROPERTIES']['TYPE']['VALUE']): ?>
                        <li><?=$arResult['PROPERTIES']['TYPE']['VALUE'];?></li>
                        <? endif; ?>
                        <li><?=strtolower($arResult["DISPLAY_ACTIVE_FROM"]);?></li>
                    </ul>
                    <h1><?=$arResult['NAME'];?></h1>
                </div>
                <? endif; ?>
            </div>
        </div>

        <article class="text_block">
        <? if (strlen($arResult["DETAIL_TEXT"]) > 0): ?>
            <?=$arResult["DETAIL_TEXT"];?>
        <? else: ?>
            <?=$arResult["PREVIEW_TEXT"];?>
        <? endif; ?>
        </article>

        <div class="news-bottom row align-items-center promo-item">
            <? if ($arResult['PROPERTIES']['PRODUCTS']['VALUE']): ?>
                <? foreach ($arResult["PRODUCTS"] as $key => $arProduct): ?>
                    <div class="col-6 col-sm-3 promo-item-product">
                        <a href="<?=$arProduct['DETAIL_PAGE_URL'];?>">
                            <img class="mb-2" src="<?=$arProduct['IMAGE_SRC'];?>" alt="<?=$arProduct["NAME"]?>" width="100%">
                            <p class="promo-item-product-title"><?=$arProduct['NAME']?></p>
                            <p class="promo-item-product-price"><?=$arProduct["PRICE"]?></p>
                        </a>
                    </div>
                <? endforeach; ?>
            <? endif; ?>
        </div>

    </div>
</section>
