<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$res = CIBlockElement::GetByID($arResult["ID"])->fetch();
$arResult["DATE_ACTIVE_FROM"] = \Intek\Format::Dates($res['ACTIVE_FROM']);
$arResult["DATE_ACTIVE_TO"] = \Intek\Format::Dates($res['ACTIVE_TO']);

if ($arResult['PROPERTIES']['PRODUCTS']['VALUE']) {
    foreach ($arResult['PROPERTIES']['PRODUCTS']['VALUE'] as $arProductID) {
        $product = CIBlockElement::GetByID($arProductID)->GetNext();
        $image = \CFile::ResizeImageGet($product['DETAIL_PICTURE'], array(), BX_RESIZE_IMAGE_PROPORTIONAL_ALT, false);
        $product['IMAGE_SRC'] = $image['src'];

        $db_res = CPrice::GetList(array(), array("PRODUCT_ID" => $arProductID))->Fetch();
        $product['PRICE'] = CurrencyFormat($db_res["PRICE"], $db_res["CURRENCY"]);

        $arResult['PRODUCTS'][] = $product;
    }
    // shuffle($arResult['ITEMS'][$key]['PRODUCTS']);
}
// Bitrix\Main\Diag\Debug::dumpToFile(array(date("Y.m.d H:i:s"), $ar_res),"","/local/logs/test.txt");
