<?php
define("NO_KEEP_STATISTIC", true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Loader;
Loader::includeModule("iblock");
Loader::includeModule("catalog");

global $USER;

while (ob_get_level()) {
	ob_end_flush();
}

if (!function_exists('getallheaders')) {
    function getallheaders()
    {
        $headers = array ();
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) == 'HTTP_') {
                $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
            }
        }
        return $headers;
    }
}
$headers = getallheaders();
if ($headers["Content-Type"] == "application/json") {
    $str_json = json_decode(file_get_contents('php://input'), true) ?: [];
    processProduct($str_json, $str_json['stocks']);
}

function sectionExist($category_xmlid)
{
	$arOrder = array('ID' => 'asc');
	$arFilter = array('IBLOCK_ID' => 3, 'XML_ID' => $category_xmlid);
	$arSection = CIBlockSection::GetList($arOrder, $arFilter, true, array("ID", "PICTURE"))->Fetch();
	// Bitrix\Main\Diag\Debug::dumpToFile(array(date("Y.m.d H:i:s"), $arSection),"","/local/logs/api.txt");

	if ($arSection['ID'] > 0) {
		return $arSection;
	} else {
		return false;
	}
	echo "Fuck Up!";
}

function createCategory($category, $sectionId=null)
{
	$section = new CIBlockSection;

	$arParams = array("replace_space" => "-", "replace_other" => "-");
	$sectionCode = Cutil::translit($category['title'], "ru", $arParams);
	@exif_imagetype('https://intekopt.ru/upload/sections/'.$category['xmlid'].'.gif') ? $imageSection = $category['xmlid'].'.gif' : $imageSection = $category['xmlid'].'.jpg';

	$arSecFields = array(
		'IBLOCK_ID' => 3,
		'NAME' => $category['title'],
		"CODE" => $sectionCode,
		"ACTIVE" => 'Y',
		"IBLOCK_SECTION_ID" => $sectionId,
		"XML_ID" => $category['xmlid'],
		"PICTURE" => CFile::MakeFileArray("https://intekopt.ru/upload/sections/".$imageSection),
	);

	$sectionIdNew = $section->Add($arSecFields);

	return $sectionIdNew;
}

function processCategory($category)
{
	$path = '';
	$parenSectionId = 0;
	$iblock_section_id = 0;

	// если (есть родительская категория)
	if (isset($category['parent'])) {
		// Обработать категорию продукта через метод processCategory
		$parenSectionId = processCategory($category['parent']);
	}
	// $test = gettype($category['xmlid']);
	$iblock_section = sectionExist($category['xmlid']);
	$iblock_section_id = $iblock_section['ID'];
	// если (Секция не существует в каталоге){
	if (!$iblock_section_id) {
		// 	Созать секцию из категории
		// Bitrix\Main\Diag\Debug::dumpToFile(array(date("Y.m.d H:i:s"), "create - ".$iblock_section_id),"","/local/logs/api.txt");
		$iblock_section_id = createCategory($category, $parenSectionId);
	}
	// elseif (!$iblock_section['PICTURE']) {
	// 	$bs = new CIBlockSection;
	// 	if (@exif_imagetype('https://intekopt.ru/upload/sections/'.$category['xmlid'].'.gif')) {
	// 		$imageSection = $category['xmlid'].'.gif';
	// 	} elseif (@exif_imagetype('https://intekopt.ru/upload/sections/'.$category['xmlid'].'.jpg')) {
	// 		$imageSection = $category['xmlid'].'.jpg';
	// 	}
	// 	if ($imageSection) {
	// 		Bitrix\Main\Diag\Debug::dumpToFile(array(date("Y.m.d H:i:s"), "update - ".$iblock_section_id, $iblock_section['PICTURE']),"","/local/logs/api.txt");
	// 		$bs->Update($iblock_section_id, array("PICTURE" => CFile::MakeFileArray("https://intekopt.ru/upload/sections/".$imageSection)));
	// 	}
	// }

	return $iblock_section_id;
}

function translateInvToImg($invCode)
{
	$invCode = trim($invCode);
	$nameFile = "";

	for ($num = 0; $num < strlen($invCode); $num++) {
		mb_internal_encoding("UTF-8");
		$symbol = mb_substr($invCode, $num, 1);
		$symbol = mb_strtolower($symbol);
		if ($symbol == "й") {
			$nameFile .= "iy";
		} elseif ($symbol == "ц") {
			$nameFile .= "cc";
		} elseif ($symbol == "у") {
			$nameFile .= "u";
		} elseif ($symbol == "к") {
			$nameFile .= "k";
		} elseif ($symbol == "е") {
			$nameFile .= "e";
		} elseif ($symbol == "н") {
			$nameFile .= "n";
		} elseif ($symbol == "г") {
			$nameFile .= "g";
		} elseif ($symbol == "ш") {
			$nameFile .= "h";
		} elseif ($symbol == "щ") {
			$nameFile .= "dg";
		} elseif ($symbol == "з") {
			$nameFile .= "z";
		} elseif ($symbol == "х") {
			$nameFile .= "x";
		} elseif ($symbol == "ъ") {
			$nameFile .= "dl";
		} elseif ($symbol == "ф") {
			$nameFile .= "f";
		} elseif ($symbol == "ы") {
			$nameFile .= "y";
		} elseif ($symbol == "в") {
			$nameFile .= "v";
		} elseif ($symbol == "а") {
			$nameFile .= "a";
		} elseif ($symbol == "п") {
			$nameFile .= "p";
		} elseif ($symbol == "р") {
			$nameFile .= "r";
		} elseif ($symbol == "о") {
			$nameFile .= "o";
		} elseif ($symbol == "л") {
			$nameFile .= "l";
		} elseif ($symbol == "д") {
			$nameFile .= "d";
		} elseif ($symbol == "ж") {
			$nameFile .= "j";
		} elseif ($symbol == "э") {
			$nameFile .= "w";
		} elseif ($symbol == "я") {
			$nameFile .= "ya";
		} elseif ($symbol == "ч") {
			$nameFile .= "ch";
		} elseif ($symbol == "с") {
			$nameFile .= "s";
		} elseif ($symbol == "м") {
			$nameFile .= "m";
		} elseif ($symbol == "и") {
			$nameFile .= "i";
		} elseif ($symbol == "т") {
			$nameFile .= "t";
		} elseif ($symbol == "ь") {
			$nameFile .= "zz";
		} elseif ($symbol == "б") {
			$nameFile .= "b";
		} elseif ($symbol == "ю") {
			$nameFile .= "q";
		} elseif ($symbol == "\\") {
			$nameFile .= "ls";
		} elseif ($symbol == "/") {
			$nameFile .= "ps";
		} elseif ($symbol == "1") {
			$nameFile .= "1";
		} elseif ($symbol == "2") {
			$nameFile .= "2";
		} elseif ($symbol == "3") {
			$nameFile .= "3";
		} elseif ($symbol == "4") {
			$nameFile .= "4";
		} elseif ($symbol == "5") {
			$nameFile .= "5";
		} elseif ($symbol == "6") {
			$nameFile .= "6";
		} elseif ($symbol == "7") {
			$nameFile .= "7";
		} elseif ($symbol == "8") {
			$nameFile .= "8";
		} elseif ($symbol == "9") {
			$nameFile .= "9";
		} elseif ($symbol == "0") {
			$nameFile .= "0";
		}
	}

	// $nameFile .= ".gif";
	return $nameFile;
}

function createProduct($product, $stock)
{
	$bs = new CIBlockSection;
	$bs->Update($product['IBLOCK_SECTION_ID'], array('ACTIVE' => 'Y'));

    $el = new CIBlockElement;

	$active = ($stock['quantity'] > 0 && $stock['quantity'] > trim($product['multiplicity'])) ? 'Y' : 'N';

	$PROP = array();
	$PROP['INV_NUM'] = trim($product['invcode']);
    $imageProduct = translateInvToImg(trim($product['invcode']));
		// Bitrix\Main\Diag\Debug::dumpToFile(array(date("Y.m.d H:i:s"), $PROP),"","/local/logs/api.txt");
	$arLoadProductArray = array(
		"MODIFIED_BY"    	=> 'CatalogLoader', //$USER->GetID(), // элемент изменен текущим пользователем
		"IBLOCK_SECTION_ID" => $product['IBLOCK_SECTION_ID'],          // элемент лежит в корне раздела
		"IBLOCK_ID"      	=> 3,
		"PROPERTY_VALUES"	=> $PROP,
		"NAME"           	=> trim($product['title']),
		"ACTIVE"         	=> $active,            // активен
		"DETAIL_TEXT"    	=> $product['description'],
		"XML_ID"		 	=> $product['xmlid'],
	);

	if($PRODUCT_ID = $el->Add($arLoadProductArray)) {
		//echo "New ID: ".$PRODUCT_ID;
		if ($PRODUCT_ID) {
			// echo "\n"."CREATE product ID = " . $PRODUCT_ID . "\n";
            $arProdFields = array(
                "ID" => $PRODUCT_ID,
                "VAT_INCLUDED" => "Y",
				"QUANTITY" => $stock['quantity']
            );
            CCatalogProduct::Add($arProdFields);
            CPrice::SetBasePrice($PRODUCT_ID, $stock['price'], 'RUB');
			@exif_imagetype('https://intekopt.ru/upload/photo/'.$imageProduct.'/0.jpg') ? $imageProduct = $imageProduct.'/0.jpg' : $imageProduct = $imageProduct.'/0.gif';
			$res = $el->Update($PRODUCT_ID, array("DETAIL_PICTURE" => CFile::MakeFileArray("https://intekopt.ru/upload/photo/".$imageProduct)));
        }
	} else {
		echo "Error: ".$el->LAST_ERROR;
	}
	return 0;
}

function updateProduct($product, $stock)
{
	$el = new CIBlockElement;
	$active = ($stock['quantity'] > 0 && $stock['quantity'] > trim($product['multiplicity'])) ? 'Y' : 'N';
	$el->Update($product['PRODUCT_ID'], array("ACTIVE" => $active));
    CCatalogProduct::Update($product['PRODUCT_ID'], array("QUANTITY" => $stock['quantity']));
	$resPrices = CPrice::GetList(
        array(),
        array(
            "PRODUCT_ID" => $product['PRODUCT_ID']
        )
    );

	if ($arrPrices = $resPrices->Fetch()) {
		CPrice::Update($arrPrices["ID"], array("PRICE" => $stock['price']));
	}

	return 0;
}

function processProduct($product, $stock)
{
    // Bitrix\Main\Diag\Debug::writeToFile(array(date("Y.m.d H:i:s"), $product, $stock),"","/local/logs/zt_log_cross_docking.txt");
    $arSelect = array("ID");
	$arFilter = array("IBLOCK_ID"=> 3, "XML_ID" => $product['xmlid']);
	$res = CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect)->Fetch();
	// $iblock_section_id = processCategory($product['category']);
	// $product['IBLOCK_SECTION_ID'] = $iblock_section_id;
	// $product['IBLOCK_SECTION_ID'] = sectionExist($product['category']);
    if ($res['ID']) {
        $product['PRODUCT_ID'] = $res['ID'];
		updateProduct($product, $stock);
    } else {
		$product['IBLOCK_SECTION_ID'] = processCategory($product['category']);
		createProduct($product, $stock);
    }
	return 0;
}
